# Operational Readiness Checklist

This repository contains [Production Readiness Checklists](docs/references/production-readiness-checklist.md) and related [documentation](/docs) to be used internally during the process of microservices development and deployment it production-ready (service is ready for live customer requests).
The checklists are divided into 2 phases:

- [Design Checklist](/docs/references/design-checklist.md): the checklist you must meet before beginning development of microservice
- [Pre-production Checklist](/docs/references/pre-production-checklist.md): the checklist you must meet before production deployment

The check items in each phase vary by its [Production Readiness Level](/docs/references/production-readiness-level.md) which is defined by its SLO. You can see the guide [Check Production Readiness](/docs/guides/check-production-readiness.md) to know checklist usage and its review process. 
